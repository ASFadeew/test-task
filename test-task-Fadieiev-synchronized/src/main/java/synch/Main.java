package synch;

public class Main {
    public static void main(String[] args) {
        BankAccount account_1, account_2;
        account_1 = new BankAccount("4141");
        account_2 = new BankAccount("5632");

        account_1.setAmount(500);
        account_2.setAmount(500);

        Thread thread_1 = new Thread(new DoTransfer(account_1, account_2), "Transfer_1");
        Thread thread_2 = new Thread(new DoTransfer(account_2, account_1), "Transfer_2");
        thread_1.start();
        thread_2.start();
    }
}
