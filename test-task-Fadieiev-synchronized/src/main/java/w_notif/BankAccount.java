package w_notif;

public class BankAccount {
    private String accountID;
    private int currentAmount;

    BankAccount(String accountID) {
        this.currentAmount = 0;
        this.accountID = accountID;
    }

    void setAmount(int amount) {
        this.currentAmount = amount;
    }

    int getAmount() {
        return this.currentAmount;
    }

    String getAccountID() {
        return accountID;
    }
}
