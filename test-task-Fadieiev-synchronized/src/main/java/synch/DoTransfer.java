package synch;

public  class DoTransfer implements Runnable {
    private BankAccount fromAcc;
    private BankAccount toAcc;

    DoTransfer(BankAccount fromAcc, BankAccount toAcc) {
        this.fromAcc = fromAcc;
        this.toAcc = toAcc;
    }

    private void transfer(BankAccount fromAccount, BankAccount toAccount,  int transferAmount) {

        synchronized (fromAccount) {
            synchronized (toAccount) {
                doTransfer(fromAccount, toAccount, transferAmount);
            }
        }
    }
     private void doTransfer(BankAccount fromAccount, BankAccount toAccount, int amount) {
         if (fromAccount.getAmount() >= amount) {
             //System.out.println("Поток: " + Thread.currentThread().getName());
             //System.out.println("На счету " + fromAccount.getAccountID() + " до снятия " + fromAccount.getAmount());
             fromAccount.withdraw(amount);
             //System.out.println("На счету " + fromAccount.getAccountID() + " после снятия " + fromAccount.getAmount());
             //System.out.println("На счету "  + toAccount.getAccountID() + " до зачисления " + toAccount.getAmount());
             toAccount.deposit(amount);
             //System.out.println("На счету "  + toAccount.getAccountID() + " после зачисления " + toAccount.getAmount());
         }
     }

    public void run() {
        transfer(this.fromAcc, this.toAcc, 150);
    }
}
