package synch;

public class BankAccount implements Comparable<BankAccount> {
    private String accountID;
    private int currentAmount;

    public BankAccount(String accountID) {
        this.currentAmount = 0;
        this.accountID = accountID;
    }

    public void setAmount (int amount) {
        this.currentAmount = amount;
    }

    int getAmount() {
        return this.currentAmount;
    }

    String getAccountID() {
        return this.accountID;
    }

    void withdraw(int amount) {
        currentAmount = currentAmount - amount;
    }

    void deposit(int amount) {
        currentAmount = currentAmount + amount;
    }

    public int compareTo(BankAccount bankAccount) {
        return this.accountID.compareTo(bankAccount.accountID);
    }
}
