package w_notif;

public class DoTransfer implements Runnable {
    private BankAccount fromAcc;
    private BankAccount toAcc;
    private boolean chek = true;

    DoTransfer(BankAccount fromAcc, BankAccount toAcc) {
        this.fromAcc = fromAcc;
        this.toAcc = toAcc;
    }

    private void transfer(BankAccount fromAccount, BankAccount toAccount, int transferAmount) {

        if (fromAccount.getAmount() >= transferAmount) {
            //System.out.println("Поток: " + Thread.currentThread().getName() + " На счету [" + fromAccount.getAccountID() + "] до снятия " + fromAccount.getAmount());
            withdraw(fromAccount, transferAmount);
            //System.out.println("Поток: " + Thread.currentThread().getName() + " На счету [" + fromAccount.getAccountID() + "] после снятия " + fromAccount.getAmount());
            //System.out.println("Поток: " + Thread.currentThread().getName() + " На счету [" + toAccount.getAccountID() + "] до зачисления " + toAccount.getAmount());
            deposit(toAccount, transferAmount);
            //System.out.println("Поток: " + Thread.currentThread().getName() + " На счету [" + toAccount.getAccountID() + "] после зачисления " + toAccount.getAmount());
        } else {
            System.out.println("Insufficient funds");
        }
    }

    private synchronized void withdraw(BankAccount account, int amount) {
        while (!chek) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println("InterruptedException перехвачено!");
            }
        }
        account.setAmount(account.getAmount() - amount);
        chek = false;
        notify();
    }

    private synchronized void deposit(BankAccount account, int amount) {
        while (chek) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println("InterruptedException перехвачено!");
            }
        }
        account.setAmount(account.getAmount() + amount);
        chek = true;
        notify();
    }

    public void run() {
        transfer(this.fromAcc, this.toAcc, 150);
    }
}
